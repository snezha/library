CREATE TABLE 'category' (
  'id' int(11) NOT NULL,
  'name' varchar(255),
  PRIMARY KEY ('id')
);

CREATE TABLE 'author' (
  'id' int(11) NOT NULL,
  'name' varchar(255),
  PRIMARY KEY ('id')
);

CREATE TABLE 'book' (
  'id' int(11) NOT NULL,
  'name' varchar(255),
  'id_author' int(11),
  'id_category' int(11),
  'description' varchar(2048),
  PRIMARY KEY ('id'),
  KEY 'id_author' ('id_author'),
  KEY 'id_category' ('id_category'),
  CONSTRAINT 'book_ibfk_1' FOREIGN KEY ('id_author') REFERENCES 'author' ('id'),
  CONSTRAINT 'book_ibfk_2' FOREIGN KEY ('id_category') REFERENCES 'category' ('id')
);

CREATE TABLE 'student' (
  'id' int(11) NOT NULL,
  'name' varchar(255),
  'firstname' varchar(255),
  PRIMARY KEY ('id')
);

CREATE TABLE 'journal' (
  'id' int(11) NOT NULL,
  'date_take' date,
  'date_back' date,
  'id_student' int(11) NOT NULL,
  'id_book' int(11) NOT NULL,
  PRIMARY KEY ('id'),
  KEY 'journal_FK1' ('id_book'),
  KEY 'journal_FK2' ('id_student'),
  CONSTRAINT 'journal_FK1' FOREIGN KEY ('id_book') REFERENCES 'book' ('id'),
  CONSTRAINT 'journal_FK2' FOREIGN KEY ('id_student') REFERENCES 'student' ('id')
);
