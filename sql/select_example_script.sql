--Предположим студент (id=3) взял 3 книги (id=1,2,3).
--Допустим, надо узнать, кто брал эти же книги с 1 января по 5 января (кроме самого студента).

SELECT student.name, student.firstname, book.name, book.date_take 
FROM student 
	INNER JOIN journal ON student.id=journal.id_student
    INNER JOIN book ON book.id=journal.id_book
WHERE id_book IN (1,2,3)
AND date_take>='2017-01-01'
AND date_take<='2017-01-05'
AND student.id!=3;
