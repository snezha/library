# coding: utf-8
from .. import db

from ..main.models import Student
from ..main.admin import StudentAdmin

from ..main.models import Book
from ..main.admin import BookAdmin

from app.main.models import Category
from app.main.admin import CategoryAdmin

from app.main.models import Journal
from app.main.admin import JournalAdmin

from app.main.models import Author
from app.main.admin import AuthorAdmin

    
def init_admin(Admin, app):
    admin = Admin(app,
                  u"Library",)

    admin.add_view(StudentAdmin(Student,
                                db.session,
                                name=u'Студенты'))

    admin.add_view(CategoryAdmin(Category,
                             db.session,
                             name=u'Категории', category='Информация о книгах'))

    admin.add_view(AuthorAdmin(Author,
                             db.session,
                             name=u'Авторы', category='Информация о книгах'))

    admin.add_view(BookAdmin(Book,
                             db.session,
                             name=u'Книги', category='Информация о книгах'))

    admin.add_view(JournalAdmin(Journal,
                             db.session,
                             name=u'Журнал'))

    return admin
