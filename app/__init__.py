# coding: utf-8

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from config import config
from flask_admin import Admin
from flask_script import Manager

bootstrap = Bootstrap()
db = SQLAlchemy()
manager = Manager()


#  создание экземпляра приложения с конфигурацией config_name. Вызов происходит из manage.py
def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    db.init_app(app)

    from .cms.admin_cms import init_admin
    init_admin(Admin, app)

    from .main import main as main
    app.register_blueprint(main)

    from .cms import cms as cms
    app.register_blueprint(cms)

    return app
