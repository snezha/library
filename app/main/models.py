# -*- coding: utf-8 -*-
from .. import db

from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import ForeignKey
from sqlalchemy import Boolean
from sqlalchemy import Text
from sqlalchemy.sql import func
from sqlalchemy import Date
from sqlalchemy import Column
from sqlalchemy import Unicode
from sqlalchemy.orm import relationship


class Student(db.Model):
    __tablename__ = 'student'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))  # имя
    firstname = Column(String(255))  # фамилия

    def __repr__(self):
        return '<Student = %s>' % self.name

    def __str__(self):
        return self.name


class Category(db.Model):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))  # Название

    def __repr__(self):
        return '<Category = %s >' % self.name

    def __str__(self):
        return self.name


class Author(db.Model):
    __tablename__ = 'author'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))  # ФИО

    def __repr__(self):
        return '<Author = %s >' % self.name

    def __str__(self):
        return self.name


class Book(db.Model):
    __tablename__ = 'book'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))  # Название
    id_author = Column(Integer, ForeignKey(Author.id))  # Автор
    author = relationship("Author")
    id_category = Column(Integer, ForeignKey(Category.id))  # Категория
    category = relationship("Category")
    description = Column(String(2048))  # Описание книги

    def __repr__(self):
        return '<Book = %s %s>' % (self.name, self.author)

    def __str__(self):
        return self.name


class Journal(db.Model):
    __tablename__ = 'journal'
    id = Column(Integer, primary_key=True)
    date_take = Column(Date)
    date_back = Column(Date)
    id_student = Column(Integer, ForeignKey(Student.id))
    student = relationship("Student")
    id_book = Column(Integer, ForeignKey(Book.id))
    book = relationship("Book")

    def __repr__(self):
        return '<Journal (%s| %d - %d)>' % (self.date_take, self.id_student, self.id_book)

    def __str__(self):
        return self.name