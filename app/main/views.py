# coding: utf-8
from . import main
from app import db
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from app.main.models import Book
from app.main.models import Category
from app.main.models import Author
import markdown


@main.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        book = Book.query.filter_by(id=request.form['book_id']).first()
        book.description = request.form['description']
        db.session.commit()
        return redirect(url_for('main.index'))
    books = Book.query.all()
    categories = Category.query.all()
    authors = Author.query.all()
    authors_dict = dict()
    category_dict = dict()
    for category in categories:
        category_dict[category.id] = category
    for author in authors:
        authors_dict[author.id] = author

    result = dict()
    for book in books:
        if book.id_category not in result:
            result[book.id_category] = {}
        if book.id_author not in result[book.id_category]:
            result[book.id_category][book.id_author] = [(book, markdown.markdown(book.description))]
        else:
            result[book.id_category][book.id_author].append((book, markdown.markdown(book.description)))
    return render_template('index.html', title=u'Библиотека', authors_dict=authors_dict, category_dict=category_dict,
                           result=result)
