# coding: utf-8
from flask_admin.contrib import sqla


class StudentAdmin(sqla.ModelView):
    column_searchable_list = ['name', 'firstname']
    column_filters = ['name', 'firstname']

    form_columns = ['name',
                    'firstname',
                    ]

    column_labels = dict(
        name=u'Имя студента',
        firstname=u'Фамилия студента',
    )


class BookAdmin(sqla.ModelView):
    column_searchable_list = ['name', 'id_author', 'id_category', 'description']
    column_filters = ['name', 'id_author', 'id_category', 'description']

    form_columns = ['name',
                    'author',
                    'category',
                    'description',
                    ]

    column_labels = dict(
        name=u'Название книги',
        author=u'Автор книги',
        category=u'Категория',
        description=u'Описание книги'
    )


class CategoryAdmin(sqla.ModelView):
    column_searchable_list = ['name']
    column_filters = ['name']

    form_columns = ['name',
                    ]

    column_labels = dict(
        name=u'Название категории',
    )


class AuthorAdmin(sqla.ModelView):
    column_searchable_list = ['name']
    column_filters = ['name']

    form_columns = ['name',
                    ]

    column_labels = dict(
        name=u'ФИО автора',
    )


class JournalAdmin(sqla.ModelView):
    column_searchable_list = ['date_take',
                              'date_back',
                              ]
    column_filters = ['date_take',
                      'date_back',
                      ]

    form_columns = ['date_take',
                    'date_back',
                    'student',
                    'book'
                    ]

    column_labels = dict(
        date_take=u'Дата взятия',
        date_back=u'Дата возврата',
        student=u'Студент',
        book=u'Книга',
    )